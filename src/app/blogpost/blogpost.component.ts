import { Component, OnInit } from '@angular/core';
import { Posts } from '../interfaces/posts';
import { PostsService } from '../posts.service';
import { Users } from '../interfaces/users';
import {catchError } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { throwError, Observable } from 'rxjs';
import { Comments } from '../interfaces/comments';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-blogpost',
  templateUrl: './blogpost.component.html',
  styleUrls: ['./blogpost.component.css']
})
export class BlogpostComponent implements OnInit {

  postsdata$: Posts[]=[];
  commentsdata$: Comments[] =[];
  author:string;
  authorMail:string;
  message:string;
  userID:string;


  constructor(private postsservice:PostsService, public authService:AuthService, public router:Router) { }
  
  private handleError(res:HttpErrorResponse){
    console.log(res.error);
    return throwError("res.error")  }

  ngOnInit() {
    this.postsservice.getPosts().subscribe(data =>this.postsdata$ = data);
   this.postsservice.getComments().subscribe(data =>this.commentsdata$ = data);

   this.authService.getUser().subscribe(        // מייבא את מספר המשתמש
    user=>
    {
      this.userID = user.uid
      console.log('userID: ', this.userID);
    }
  );

  }
  savePost(title:string,body:string){
    this.postsservice.savePost(this.userID,title,body)
    console.log("Post saved");
    this.router.navigate(['/savedpost']);
    //this.message= "Saved for later viewing"
  }


}
