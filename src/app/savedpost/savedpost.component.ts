import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';
import { PostsService } from '../posts.service';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-savedpost',
  templateUrl: './savedpost.component.html',
  styleUrls: ['./savedpost.component.css']
})
export class SavedpostComponent implements OnInit {

  constructor(private postsservice:PostsService, public authService:AuthService, public router:Router) { }
  poosts:any;
  posts$:Observable<any>;
  userId:string;


  ngOnInit() {
    this.authService.user.subscribe(
      user => {
        this.userId = user.uid;
        this.posts$ = this.postsservice.getUserPost(this.userId);
      }
    )
  }

  
  deletebook(id:string){
    this.postsservice.deletepost(id , this.userId);
  }

  likepost(id:string, likes:string){
    this.postsservice.likepost( id , this.userId, likes);
  }

}
