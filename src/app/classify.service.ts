import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import {AngularFirestore, AngularFirestoreCollection, AngularFirestoreCollectionGroup } from '@angular/fire/firestore';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ClassifyService {

  constructor(private http:HttpClient, private db:AngularFirestore,public router:Router) { }
  private url = " https://xmg1ec1wvh.execute-api.us-east-1.amazonaws.com/beta";
  userCollection: AngularFirestoreCollection = this.db.collection('users');
  public categories:object = {0: 'business', 1: 'entertainment', 2: 'politics', 3: 'sport', 4: 'tech'};
  public doc:string;


  classify():Observable<any>{
    let json = {
      "articles": [
        {"text": this.doc}
      ]
    }
    
    let body = JSON.stringify(json);
    return this.http.post<any>(this.url,body).pipe(
      map(res => {
        let final = res.body.replace('[', '');
        final = final.replace(']', '');
        return final;
      })
    )
  }

  addClassification(userID:string, doc:string,category:string)
  {
    const article={ userID:userID, doc:doc, category:category}

    this.userCollection.doc(userID).collection('Articles').add(article);
    console.log("article: " ,article)
    this.router.navigate(['/articles'])    
  }
  

}


