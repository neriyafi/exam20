import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Posts } from './interfaces/posts';
import { Comments } from './interfaces/comments';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class PostsService {


private POSTURL="https://jsonplaceholder.typicode.com/posts"
private commentsURL="https://jsonplaceholder.typicode.com/comments/"

constructor(private http:HttpClient, private db:AngularFirestore) { }
Numberoflikes:string;
userCollection: AngularFirestoreCollection = this.db.collection('users');

    getPosts():Observable<Posts[]>{
      console.log("in getPosts ")
    return this.http.get<Posts[]>(`${this.POSTURL}`);
    }

   getComments():Observable<Comments[]>{
    console.log("in getComments ")
    return this.http.get<Comments[]>(`${this.commentsURL}`);
    }

   savePost(userID:string , title:string, body:string, ){
      const post = {userID:userID, title:title, body:body, likes:0}
      this.userCollection.doc(userID).collection('posts').add(post);
      console.log("post: " ,post)
    }

    getUserPost(userId:string):Observable<any[]>{
      // return this.db.collection('books').valueChanges({idField:'id'});
      console.log("in getUserPost ")
      this.userCollection = this.db.collection(`users/${userId}/posts`);
       return this.userCollection.snapshotChanges().pipe(
         map(
           collection => collection.map(
             document => {
               const data = document.payload.doc.data();
               data.id = document.payload.doc.id;
               return data;
             }
           )
     
         )
       )
     
     }
     
     deletepost(id:string , userId:string){
     
      this.db.doc(`users/${userId}/posts/${id}`).delete();
     }

     likepost(id:string , userId:string, likes){

      this.Numberoflikes = likes+1;
      this.db.doc(`users/${userId}/posts/${id}`).update({
        likes: this.Numberoflikes,

      })
     }
     
     

}
