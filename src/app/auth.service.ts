import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from './interfaces/user';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router, ActivatedRoute } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  user: Observable<User | null>
  constructor(public afAuth:AngularFireAuth,private router:Router, private route:ActivatedRoute ) { this.user = this.afAuth.authState}

  signup(email:string , password:string){
    this.afAuth
      .auth
      .createUserWithEmailAndPassword(email,password)
      .then(
        (res) => 
        {
          console.log("signup successfully")
          this.router.navigate(['/welcome'])
        }
      )
      .catch(function(error) 
                          {
                            var errorCode = error.code;
                            var errorMessage = error.message;
                            if (errorCode) {
                              alert(errorMessage);
                            } else {
                              alert(errorMessage);
                            }
                          });
                          
  }

  logout(){
    this.afAuth.auth.signOut().then(res => console.log('Yahuuu', res));
    this.router.navigate (['/'])
  }

  login(email:string, password:string){
    this.afAuth.auth.signInWithEmailAndPassword(email,password).then(
      (res) => 
      {
        console.log("Login successfully")
        this.router.navigate(['/welcome'])
      }
    )
    .catch(function(error) 
                        {
                          // Handle Errors here.
                          var errorCode = error.code;
                          var errorMessage = error.message;
                          if (errorCode) {
                            alert(errorMessage);
                          } else {
                            alert(errorMessage);
                          }
                        });  
                      
    }

    getUser()
    {
      //console.log("user Observable: ",this.user)
      return this.user;
    }
  }
 
