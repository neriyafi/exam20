// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyC9hGlJYAO6alBejGXXrgpccu6_GBSt9q0",
    authDomain: "exambbc-96235.firebaseapp.com",
    databaseURL: "https://exambbc-96235.firebaseio.com",
    projectId: "exambbc-96235",
    storageBucket: "exambbc-96235.appspot.com",
    messagingSenderId: "183991476194",
    appId: "1:183991476194:web:6742a7e13db819e65023bb"
  }
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
